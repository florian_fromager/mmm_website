"use strict";

var _vue = require("vue");

var _Main = _interopRequireDefault(require("./Main.vue"));

var _router = _interopRequireDefault(require("./router"));

var _vueMobileDetection = _interopRequireDefault(require("vue-mobile-detection"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var main = (0, _vue.createApp)(_Main["default"]);
var url = 'https://mmm-db.herokuapp.com';
main.config.globalProperties.herokUrl = url;
main.use(_router["default"]);
main.use(_vueMobileDetection["default"]);
main.mount('#main');